package com.microservice.productservice.service;

import com.microservice.productservice.model.Product;
import com.microservice.productservice.repository.ProductRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private Product product;

    @Mock
    private Iterable<Product> products;

    @Before
    public void setUp(){
        product = Product.builder().name("teste").build();
    }

    @Test
    public void save(){

        when(productService.save(any())).thenReturn(product);

        Product productResult = productService.save(product);

        verify(productRepository, times(1)).save(any());

        assertEquals(product, productResult);
    }

    @Test
    public void findById(){
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));

        Product productResult = productService.findById(1L);

        verify(productRepository, times(1)).findById(anyLong());

        assertEquals(product, productResult);
    }

    @Test
    public void findByIdNull(){

        Optional<Product> productOptional = Optional.empty();

        when(productRepository.findById(anyLong())).thenReturn(productOptional);

        Product productResult = productService.findById(1L);

        verify(productRepository, times(1)).findById(anyLong());

        Assert.assertNull(productResult);

    }

    @Test
    public void delete(){
        productService.delete(1L);
        verify(productRepository, times(1)).deleteById(anyLong());
    }

    @Test
    public void listAll(){
        products = new ArrayList<>();
        when(productRepository.findAll()).thenReturn((List<Product>) products);
        productService.listAll();
        verify(productRepository, times(1)).findAll();
    }
}
