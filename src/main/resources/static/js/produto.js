new Vue({
    el:'#app',
    data:{
        message:'',
        alertcallback: false,
        typecallback: '',
        modalTxtAction:'',
        modalBtnAction: '',
        modalAction:'',
        product:{},
        products:[],
        productError:{name: '', description: ''},
        imageData:'',
    },
    created:function(){
        this.listAll(0,5);
    },
    methods:{
        changeModalSave: function(){
            if(!this.checkForm()){
                return;
            };

            this.modalTxtAction =  'Deseja salvar este produto?'
            this.modalBtnAction = 'salvar';
            this.modalAction = 'save';
            this.toggleModalConfirm();
        },
        changeModalDelete: function(id){
            this.modalTxtAction = 'Deseja excluir este produto?'
            this.modalBtnAction = 'confirmar';
            this.modalAction = 'delete';
            this.product.id = id;
            this.toggleModalConfirm();

        },
        toggleModalConfirm: function () {
            $('#modal-confirm-action').modal('toggle');
        },
        toggleModalProduct: function () {
            $('#modal-form-product').modal('toggle');
        },
        toggleModal: function () {
            $('#modal-confirm-action').modal('toggle');
            $('#modal-form-product').modal('toggle');
        },
        listAll:function (page) {
            this.$http.get('/product/all?page=' + page + '&size=' + 5)
                .then(function (response) {
                    this.products = response.body;
                }).catch(function (response) {
                    this.callBackForm('alert-danger', 'Erro no processamento ' + response.bodyText);
                });
        },
        findById:function (id) {
            this.$http.get('/product/' + id)
                .then(function (response) {
                    this.product = response.body;
                }).catch(function (response) {
                    this.callBackForm('alert-danger', 'Erro no processamento ' + response.bodyText);
                });
        },
        save: function(){
            this.$http.post('/product', this.product)
                .then(function (response) {
                    this.toggleModal();
                    this.callBackForm('alert-success', 'Salvo com sucesso!');
                }).catch(function (response) {
                this.callBackForm('alert-danger', 'Erro no processamento ' + response.bodyText);
            });
        },
        update: function(){
            this.$http.put('/product', this.product)
                .then(function (response) {
                    this.toggleModal();
                    this.callBackForm('alert-success', 'Salvo com sucesso!');
                }).catch(function (response) {
                this.callBackForm('alert-danger', 'Erro no processamento ' + response.bodyText);
            });
        },
        remove: function(){
            this.$http.delete('/product/' + this.product.id)
                .then(function (response) {
                    this.toggleModalConfirm();
                    this.callBackForm('alert-success', 'Removido com sucesso!');
                }).catch(function (response) {
                this.callBackForm('alert-danger', 'Erro no processamento ' + response.bodyText);
            });
        },
        cancelAction: function(){
            $('#modal-form-product').modal('show');
        },
        checkForm: function () {
            if(this.product.name && this.product.description)
                return true;

            this.productError = {};

            if(!this.product.name)
                this.productError.name = "Nome é obrigatório.";
            if(!this.product.description)
                this.productError.description = "Descrição é obrigatória.";

            return false;
        },
        callBackForm: function(type, message){
            this.listAll(0,5);

            this.alertcallback = true;
            this.message = message;
            this.typecallback = type;
            var self = this;

            setTimeout(function(){
                self.alertcallback = false;
            }, 5000);

        },
        cleanForm: function () {
            this.product = {};
        },
        uploadImage: function(event) {
            var input = event.target;

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                var self = this;

                reader.onload = function (e) {
                    self.product.image = e.target.result;
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
    }
});
