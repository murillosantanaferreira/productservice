#!/usr/bin/env bash

CMD=$1

if [ "$CMD" = "stop" ]; then
    echo "Stopping mysql"
    docker stop product
    docker rm product
else
    echo "Starting mysql"
    docker run -d --name product --network bridge -p 3306:3306 -v $(pwd):/db \
    -e MYSQL_DATABASE=product -e MYSQL_USER=user -e MYSQL_PASSWORD=123qwe \
    -e MYSQL_ROOT_PASSWORD=123qwe \
    wangxian/alpine-mysql
fi