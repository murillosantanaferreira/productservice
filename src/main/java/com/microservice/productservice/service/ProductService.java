package com.microservice.productservice.service;

import com.microservice.productservice.model.Product;
import com.microservice.productservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product save(Product product){
        return productRepository.save(product);
    }

    public Product findById(Long id){
        Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()){
            return product.get();
        }

        return null;
    }

    public void delete(Long id){
        productRepository.deleteById(id);
    }

    public List<Product> listAll(){
        return StreamSupport
                .stream(productRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Page<Product> findAll(Pageable pageable){
        Page<Product> page = productRepository.findAll(pageable);
        return page;
    }
}
